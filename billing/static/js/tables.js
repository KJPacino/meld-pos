$(document).ready(function(){
  gettables();
  $('#create-table').on('click',function(){
    var dat = {'table':$('#table').val()};
    $.ajax({
      type : 'POST',
      url : 'http://127.0.0.1:8000/billing/tables',
      data : JSON.stringify(dat),
      contentType: 'application/json',
      crossDomain:true,
      success : function(json){
        console.log(json);
        gettables();
      },
      error : function(json){
        console.log('There was an error');

      }
    });

  });

  function gettables(){
    $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/tables',
      contentType: 'application/json',
      crossDomain:true,

      success : function(json){
        for (var i = 0; i < json.length; i ++){
        $('#table-list').append('<tr>'+ '<td>'+json[i].table + '</td>'+ '</tr>');
      }
    },
      error : function(){
        console.log('there was an error with category-get');
      }
    });

  };

});
