$(document).ready(function(){

getcategoriesfororder();
gettablesfororder();

  function getcategoriesfororder(){
    $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/categories',
      contentType: 'application/json',
      crossDomain:true,

      success : function(json){
        for (var i = 0; i < json.length; i ++){
        $('#category-box-list').append('<option value = '+ json[i].pk+ '>'+json[i].category + '</option>');


      }
    },
      error : function(){
        console.log('there was an error with category-get');
      }
    });
  };

  function gettablesfororder(){

    $.ajax({

      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/tables',
      contentType: 'application/json',
      crossDomain:true,

      success : function(json){
        for (var i = 0; i < json.length; i ++){
        $('#tableBill').append('<option value = '+ json[i].table+ '>'+json[i].table + '</option>');


      }
    },
      error : function(){
        console.log('there was an error with table-get');
      }
    });
  };


$('#category-box-list').on('click',function(){
  $('#Item-box-list').empty();
  console.log('selected');
  var cat = $('#category-box-list').val();
  $.ajax({
    type :'GET',
    url :'http://127.0.0.1:8000/billing/categories/' + cat,
    contentType: 'application/json',
    crossDomain:true,
    success : function(json){

      for(var i = 0; i<json.length;i++){
      $('#Item-box-list').append('<option value ='+json[i].rate+'>'+json[i].item+'</option>');
    };
  }
  });
});


$('#Item-box-list').on('click',function(){
  $('#Order-Details').trigger("reset");
  $('#OrderItem').val($('#Item-box-list').find(':selected').text());
  $('#OrderRate').val($('#Item-box-list').find(':selected').val());

});

$('#OrderQuantity').on('change',function(){
  var qty = parseFloat($('#OrderQuantity').val());
  var tot = qty * parseFloat($('#OrderRate').val());


  $('#OrderTotalPerItem').val(tot);

});

$('#OrderPlace').on('click',function(e){
  e.preventDefault();
  var item = $('#OrderItem').val();
  var qrt = $('#OrderQuantity').val();
  var rate = $('#OrderRate').val();
  var tot = $('#OrderTotalPerItem').val();

  $('#BillList').append('<tr><td>'+item+'</td>'+'<td>'+qrt+'</td>'+'<td>'+rate+'</td>'+'<td class="total">'+tot+'</td></tr>');

});

$('#SubmitTotable').on('click', function(){
  var tbl =$('#order-table').tableToJSON();
  console.log(tbl);
  var other_data = $('#tableBill').find(':selected').val();
  var dat = {
    'orders':tbl,
    'table':other_data
  };
  console.log('table id : ' + other_data);
  $.ajax({
    type :'POST',
    url :'http://127.0.0.1:8000/billing/orders',
    data:JSON.stringify(dat),
    contentType: 'application/json',
    crossDomain:true,
    success : function(json){
      alert('your order number is : ' +json);
    }
  });
});

});
