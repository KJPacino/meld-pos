$(document).ready(function(){
  getdiscounts();
  $('#discount-submit').on('click',function(e){
    console.log('clicked');
    e.preventDefault();
    if ($('#discount_status').is(':checked')){
      var status = true;
    }else{
      var status = false;
    };
    var dat = {
      'discount_name':$('#discount_name').val(),
      'discount_type':$('#radio_for_discount_type input[type="radio"]:checked').val(),
      'discount_value':$('#discount_value').val(),
      'discount_status':status
      };
    $.ajax({
      type : 'POST',
      url : 'http://127.0.0.1:8000/billing/discount',
      data : JSON.stringify(dat),
      contentType: 'application/json',
      crossDomain:true,
      success : function(json){
        console.log('done');
        $('#discount-list').append('<tr>'+ '<td>'+dat.discount_name + '</td><td>'+dat.discount_type+'</td><td>'+dat.discount_value+"</td><td>"+dat.discount_status+'</td></tr>');
      },
      error : function(json){
        console.log('There was an error');

      }
    });

    console.log(dat);

  });

  function getdiscounts(){
    $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/discount',
      contentType: 'application/json',
      crossDomain:true,

      success : function(json){
        for (var i = 0; i < json.length; i ++){
          if(json[i].discount_type == 'P'){
            var type = 'Percentage';
          }else{
            var type = 'Flat';
          };
          if(json[i].discount_status == true){
            var status = 'Live';
          }else{
            var status = 'Not live'
          };
        $('#discount-list').append('<tr>'+ '<td>'+json[i].discount_name + '</td><td>'+type+'</td><td>'+json[i].discount_value+"</td><td>"+status+'</td></tr>');
      }
    },
      error : function(){
        console.log('there was an error with category-get');
      }
    });

  };

});
