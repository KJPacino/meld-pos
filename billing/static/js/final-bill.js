$(document).ready(function(){

  $('#cashamount').hide();
  $('#cardamount').hide();
  $('#creditlineamount').hide();
  $('#balanceamount').hide();
  $('#Payment-Modes-0').prop('checked',false);
  $('#Payment-Modes-1').prop('checked',false);
  $('#Payment-Modes-2').prop('checked',false);

  var ord = $('#ordernoforfinal').text();

$('#apply-discount').on('click',function(){
  var bill = $.trim($('#taxed-bill-amount').html());
  var disc = $('#discount-list').find(':selected').val();
  console.log(bill);

  $.ajax({
    type :'GET',
    url : 'http://127.0.0.1:8000/billing/discounts/'+disc+'/'+bill,
    // data: JSON.stringify({amount:bill}),
    contentType: 'application/json',
    crossDomain:true,
    success : function(json){
      $('#final-billed-amount').html(json);
    }
  });
});

$('#Payment-Modes-0').on('click',function(){
  console.log('clicked');
  $('#cashamount').toggle();

  $('#full-cash').on('click',function(){
    $('#cashamt').val($('#final-billed-amount').text());

  });

});
$('#Payment-Modes-1').on('click',function(){
  console.log('clicked');
  $('#cardamount').toggle();
  $('#full-card').on('click',function(){
    $('#cardamt').val($('#final-billed-amount').text());

  });

});



$('#Payment-Modes-2').on('click',function(){
  console.log('clicked');
  $('#creditlineamount').toggle();
  $('#balanceamount').toggle();

  $('#creditlineamount').on('change',function(){
    var finalbill = parseFloat($('#final-billed-amount').text());
    console.log(finalbill);
    var creditline = $('#creditline').val();
    console.log(creditline);
    $('#balanceamt').val(finalbill - creditline);
  });

});

$('#printfinal').on('click',function(){
  var payment_modes = {};
  if($('#Payment-Modes-0').is(':checked') && $('#Payment-Modes-1').is(':checked')) {
    payment_modes['cash'] = $('#cashamt').val();
    payment_modes['credit'] = $('#cardamt').val();
    console.log(payment_modes);
  }else if ($('#Payment-Modes-1').is(':checked')) {
    payment_modes['credit'] = $('#cardamt').val();
    console.log(payment_modes);
  }else if ($('#Payment-Modes-2').is(':checked')) {
    payment_modes['creditline'] = $('#creditline').val();
    payment_modes['balance'] = $('#balanceamt').val();
    payment_modes['debtor'] = $('#creditname').val();
    console.log(payment_modes);
  }else if ($('#Payment-Modes-0').is(':checked')){
    payment_modes['cash'] = $('#cashamt').val();
    console.log(payment_modes);
  }
  else {
    console.log('none selected');
    alert('click something');
  }

  $.ajax({
    type : 'POST',
    url : 'http://127.0.0.1:8000/billing/final-bill/'+ord,
    contentType: 'application/json',
    data : JSON.stringify(payment_modes),
    crossDomain:true,
    success: function(json){
      alert(json);
      window.location.href = 'http://127.0.0.1:8000/billing/liveorders'
    }

  });



});


});
