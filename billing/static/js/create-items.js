$(document).ready(function(){

getcategoriesforitems();
getItemList();
  function getcategoriesforitems(){
    $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/categories',
      contentType: 'application/json',
      crossDomain:true,

      success : function(json){
        for (var i = 0; i < json.length; i ++){
        $('#category-box').append('<option value = '+ json[i].pk+ '>'+json[i].category + '</option>');


      }
    },
      error : function(){
        console.log('there was an error with category-get');
      }
    });
  };

  function getItemList(){
    $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/billing/create-items',
      contentType: 'application/json',
      crossDomain:true,

      success: function(json){
        for (var i = 0; i < json.length; i ++){
          $('#item-list').append('<tr><td>'+json[i].category+'</td><td>'+json[i].item+'</td><td>'+json[i].rate+'</td></tr>');
        };
      }

    });
  };

$('#create').on('click', function(e){
  e.preventDefault();
  var req = {
    'category':$('#category-box').val(),
    'item':$('#Items').val(),
    'rate':$('#Rate').val()
  }
$.ajax({
  type: 'POST',
  url:'http://127.0.0.1:8000/billing/create-items',
  data : JSON.stringify(req),
  contentType: 'application/json',
  crossDomain:true,

  success : function(json){
    console.log('success');
    $('#item-list').append('<tr><td>'+req.category+'</td><td>'+req.item+'</td><td>'+req.rate+'</td></tr>');
  },
  error: function(json){
    console.log('error with POST');

  }

});


});


});
