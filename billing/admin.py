from django.contrib import admin
from .models import *

class DiscountAdmin(admin.ModelAdmin):
    fields = ('discount_name','discount_type','discount_value','discount_status')


class TaxAdmin(admin.ModelAdmin):
    fields = ('tax_detail','tax_percentage','tax_status')


admin.site.register(Discounts,DiscountAdmin)
admin.site.register(Tax,TaxAdmin)
