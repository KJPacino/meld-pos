from django.db import models

class Category(models.Model):
    category = models.CharField(max_length = 100)

    def __str__(self):
        return self.category

class Tables(models.Model):
    table = models.CharField(max_length = 100)

    def __str__(self):
        return self.table

class Items(models.Model):
    category = models.ForeignKey(Category)
    item = models.CharField(max_length = 100)
    rate = models.DecimalField(max_digits = 20,decimal_places =2)

    def __str__(self):
        return self.item

class Orders(models.Model):
    order_number = models.AutoField(primary_key = True)
    table_number = models.ForeignKey(Tables)
    amount_settled = models.BooleanField(default = False)
    payment_mode = models.CharField(max_length = 10, null = True, blank = True)
    total_amount = models.DecimalField(max_digits = 20,decimal_places =2, null = True, blank = True)
    final_amount = models.DecimalField(max_digits = 20,decimal_places =2, null = True, blank = True)

    def __str__(self):
        return str(self.order_number)

class Sales(models.Model):
    order = models.ForeignKey(Orders)
    table_number = models.ForeignKey(Tables)
    item = models.CharField(max_length = 20)
    quantity = models.IntegerField()
    rate = models.DecimalField(max_digits = 20,decimal_places =2)
    totalperitem = models.DecimalField(max_digits = 20, decimal_places = 2)
    kot_generated = models.BooleanField(default = False)

    def __str__(self):
        return self.item


class Discounts(models.Model):
    TYPES = (
    ('P','Percentage'),
    ('F','Flat'),
    )

    discount_name = models.CharField(max_length = 50)
    discount_type = models.CharField(max_length = 1, choices = TYPES)
    discount_value = models.IntegerField()
    discount_status = models.BooleanField(default = True)

    def __str__(self):
        return self.discount_name

class Tax(models.Model):
    tax_detail = models.CharField(max_length = 50)
    tax_percentage = models.DecimalField(max_digits = 10, decimal_places = 2)
    tax_status = models.BooleanField(default = True)
    def __str__(self):
        return self.tax_detail

class CashTransactions(models.Model):
    order = models.ForeignKey(Orders)
    cash_amount = models.DecimalField(max_digits = 10, decimal_places = 2)

class CreditCardTransactions(models.Model):
    order = models.ForeignKey(Orders)
    card_amount = models.DecimalField(max_digits = 10, decimal_places = 2)

class CreditLine(models.Model):
    order = models.ForeignKey(Orders)
    paid_amount = models.DecimalField(max_digits = 10, decimal_places = 2)
    balance_amount = models.DecimalField(max_digits = 10, decimal_places = 2)
    debtor = models.CharField(max_length = 100, null = True)
