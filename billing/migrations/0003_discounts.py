# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0002_sales_quantity'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discounts',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('discount_name', models.CharField(max_length=50)),
                ('discount_type', models.CharField(max_length=1, choices=[('P', 'Percentage'), ('F', 'Flat')])),
                ('discount_value', models.IntegerField()),
                ('discount_status', models.BooleanField(default=True)),
            ],
        ),
    ]
