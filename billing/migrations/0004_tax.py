# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0003_discounts'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('tax_detail', models.CharField(max_length=50)),
                ('tax_percentage', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
        ),
    ]
