# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0004_tax'),
    ]

    operations = [
        migrations.AddField(
            model_name='tax',
            name='tax_status',
            field=models.BooleanField(default=True),
        ),
    ]
