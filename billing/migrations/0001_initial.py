# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('category', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Items',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('item', models.CharField(max_length=100)),
                ('rate', models.DecimalField(decimal_places=2, max_digits=20)),
                ('category', models.ForeignKey(to='billing.Category')),
            ],
        ),
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('order_number', models.AutoField(primary_key=True, serialize=False)),
                ('amount_settled', models.BooleanField(default=False)),
                ('payment_mode', models.CharField(max_length=10, blank=True, null=True)),
                ('total_amount', models.DecimalField(blank=True, null=True, decimal_places=2, max_digits=20)),
                ('final_amount', models.DecimalField(blank=True, null=True, decimal_places=2, max_digits=20)),
            ],
        ),
        migrations.CreateModel(
            name='Sales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('item', models.CharField(max_length=20)),
                ('rate', models.DecimalField(decimal_places=2, max_digits=20)),
                ('totalperitem', models.DecimalField(decimal_places=2, max_digits=20)),
                ('kot_generated', models.BooleanField(default=False)),
                ('order', models.ForeignKey(to='billing.Orders')),
            ],
        ),
        migrations.CreateModel(
            name='Tables',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('table', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='sales',
            name='table_number',
            field=models.ForeignKey(to='billing.Tables'),
        ),
        migrations.AddField(
            model_name='orders',
            name='table_number',
            field=models.ForeignKey(to='billing.Tables'),
        ),
    ]
