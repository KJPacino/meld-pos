from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer
from rest_framework import generics
from django.views.generic import ListView
from django.shortcuts import get_object_or_404
from django.views.generic.edit import FormView
from .forms import DiscountForm
import json


class create_items(generics.ListCreateAPIView):
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'create-items.html'
    queryset = Items.objects.all()
    serializer_class = ItemSerializer


class CategoryList(generics.ListCreateAPIView):
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'create.html'
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class TableList(generics.ListCreateAPIView):
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'create.html'
    queryset = Tables.objects.all()
    serializer_class = TableSerializer

class DiscountList(generics.ListCreateAPIView):
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'create.html'
    queryset = Discounts.objects.all()
    serializer_class = DiscountSerializer

class TaxList(generics.ListCreateAPIView):
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'create.html'
    queryset = Tax.objects.all()
    serializer_class = TaxSerializer



@api_view(['GET','POST'])
def order_generator(request,order_number = None):
    if request.method =='GET':
        try:
            order = Orders.objects.get(order_number =  order_number)
            Sales_instance =  Sales.objects.filter(order = order)
            table_number = order.table_number
        except:
            order = None
            Sales_instance = None
            table_number =  None
        return render(request,'create-order.html',{'orders':Sales_instance,'orderno':order_number,
        'table_number':table_number})
    else:
        body = request.body.decode('UTF-8')
        body_data = dict(json.loads(body))
        table_number = body_data['table']
        tableInstance =  Tables.objects.get(table= table_number)
        orders = list(body_data['orders'])

        if order_number:
            order = Orders.objects.get(order_number =  order_number)
            for i in orders:
                Order_sales = Sales(order=order,table_number = tableInstance,
                item = i['Item'],
                quantity = i['Quantity'],
                rate= i['Rate'],
                totalperitem = i['Total'],
                kot_generated = True)
                Order_sales.save()
                return HttpResponse('ok')
        else:
            order = Orders(table_number = tableInstance)
            orderNo = order.order_number
            order.save()

            for i in orders:
                Order_sales = Sales(order=order,table_number = tableInstance,
                item = i['Item'],
                quantity = i['Quantity'],
                rate= i['Rate'],
                totalperitem = i['Total'],
                kot_generated = True)
                Order_sales.save()
        return HttpResponse(order.order_number)

class ItemListView(generics.ListAPIView):
    serializer_class = ItemSerializer
    renderer_classes = (JSONRenderer,BrowsableAPIRenderer)

    def get_queryset(self):
        category = get_object_or_404(Category, pk =self.args[0])
        return Items.objects.filter(category = category)

class LiveOrderListView(generics.ListAPIView):
    serializer_class = OrderSerializer
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,BrowsableAPIRenderer)
    template_name = 'live-orders.html'

    def get_queryset(self):
        return Orders.objects.filter(amount_settled = False)

@api_view(['GET','POST'])
def final_bill_generator(request,order_number):
    if request.method =='GET':
        order = Orders.objects.get(order_number =  order_number)
        Sales_instance =  Sales.objects.filter(order = order)
        table_number = order.table_number
        total_bill_amount  = 0
        for i in Sales_instance:
            total_bill_amount += i.totalperitem

        taxes = Tax.objects.filter(tax_status= True)
        taxed_amount = total_bill_amount
        for tax in taxes:
            taxed_amount += (taxed_amount * (tax.tax_percentage/100))

        discounts = Discounts.objects.filter(discount_status = True)

        return render(request,'final-bill.html',{'orders':Sales_instance,'orderno':order_number,
        'table_number':table_number,'total_bill_amount':total_bill_amount,'taxed_amount':taxed_amount,'taxes':taxes,
        'discounts':discounts})


    else:
        order = Orders.objects.get(order_number =  order_number)
        order.amount_settled = True
        order.save()
        body = request.body.decode('UTF-8')
        body_data = dict(json.loads(body))

        if 'cash' and 'credit' in body_data:
            cash_instance =  CashTransactions(order = order, cash_amount = body_data['cash'])
            credit_instance = CreditCardTransactions(order = order, card_amount = body_data['credit'])
            cash_instance.save()
            credit_instance.save()
            return HttpResponse('Cash And Credit Saved')
        elif 'cash' in body_data:
            cash_instance =  CashTransactions(order = order, cash_amount = body_data['cash'])
            cash_instance.save()
            return HttpResponse('Cash Saved')
        elif 'credit' in body_data:
            credit_instance = CreditCardTransactions(order = order, card_amount = body_data['credit'])
            credit_instance.save()
            return HttpResponse('Credit Saved')
        elif 'creditline' in body_data:
            credit_line_instance =  CreditLine(order = order,paid_amount =body_data['creditline'], balance_amount = body_data['balance'],debtor = body_data['debtor'])
            return HttpResponse('Credit line Saved')


@api_view(['GET','POST'])
def apply_discount(request,pk,amount):
    if request.method =='GET':
        discount = Discounts.objects.get(pk = pk)
        if discount.discount_type =='F':
            final_amount = float(amount) - float(discount.discount_value)
        else:
            final_amount =  float(amount) - float(float(amount) * (discount.discount_value/100))

        return HttpResponse(final_amount)
