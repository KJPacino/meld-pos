from rest_framework import serializers
from .models import *

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk','category',)

class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tables
        fields = ('pk','table',)

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Items
        fields = ('pk','category','item','rate',)

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('order_number','table_number','amount_settled','payment_mode','total_amount','final_amount')

class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discounts
        fields = ('discount_name','discount_type','discount_value','discount_status')

class TaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tax
        fields = ('tax_detail','tax_percentage','tax_status')
