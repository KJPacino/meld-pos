from django.forms import ModelForm
from .models import Discounts

class DiscountForm(ModelForm):
    class Meta:
        model = Discounts
        fields = ['discount_name','discount_type','discount_value','discount_status']
