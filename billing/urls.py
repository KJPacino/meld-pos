from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create-items$', views.create_items.as_view(), name='items'),
    url(r'^categories$', views.CategoryList.as_view(),name ='create_categories'),
    url(r'^tables$',views.TableList.as_view(),name ='create-table'),
    url(r'^orders$',views.order_generator, name='generate-order'),
    url(r'^categories/(\S+)$',views.ItemListView.as_view(),name = 'Item-list'),
    url(r'^liveorders$',views.LiveOrderListView.as_view(), name ='live-orders'),
    url(r'^orders/(?P<order_number>[\S]+)$',views.order_generator,name = 'order-detail'),
    url(r'^final-bill/(?P<order_number>[\S]+)$',views.final_bill_generator,name = 'final-bill'),
    url(r'^discounts/(?P<pk>[\S]+)/(?P<amount>[\S]+)$',views.apply_discount,name ='discounts'),
    url(r'^discount$', views.DiscountList.as_view(),name ='discount-list'),
    url(r'^tax$', views.TaxList.as_view(),name ='tax-list'),
]
